class Person {
  final String _name;
  int _age;

  Person(this._name, this._age);

  Person.ageBelow50(this._age, this._name) {
    print(_age);
  }

  int setAge() {
    return _age;
  }

  set newAge(int a) {
    _age = a;
  }
}

void main(List<String> arguments) {
  // final p = Person('abinanto', 1998);
  // // //p.name = 'abin';
  // // print(p.name);
  // print(p._age);
}
