import 'dart:io';

void main() {
  // fixed();
  // sum(10, 15);
  // var sum1 = sumReturn(12, 18);
  // print(sum1);
  // sumReq(a: 10, b: 40, c: 50);
  // passFunction(15, 25, (int a, int b) {
  //   print('the sum is ${a + b}');
  // });
  // var n1 = stdin.readLineSync();
  // var n2 = stdin.readLineSync();
  // int num1 = n1;
  // int num2 = n2.parse(stdin.readLineSync());
  usm();
  // sumFututre(15, 15);
  print('after the Future');
}

void sum(int num1, int num2) {
  print('${num1 + num2}');
}

// int sumReturn(int num1, int num2) {
//   return num1 + num2;
// }

Future<int> sumFututre(int a, int b) async {
  await Future.delayed(Duration(seconds: 3));
  print('sum future ${a + b}');
  return a + b;
}

Future<void> usm() async {
  await sumFututre(16, 24);
  print('hello hello');
}

// void fixed() {
//   print(9 + 11);
// }

// void sumReq({required int a, required int b, int c = 0}) {
//   print(a + b + c);
// }

// void passFunction(int a, int b, void Function(int, int) customSum) {
//   customSum(a, b);
// }
