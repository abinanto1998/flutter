class Animal {
  void heyHello() {
    print('organic matter');
  }
}

mixin Plants {
  void hoiHello() {
    print('also organic but no consicence');
  }
}

class Human with Animal, Plants {
  void haiHello() {
    print('has concience');
  }

  @override
  void heyHello() {
    print('heyy not organic');
  }
}

void main(List<String> args) {
  final ea = Human();
  ea.haiHello();
  ea.heyHello();
  ea.hoiHello();
}
